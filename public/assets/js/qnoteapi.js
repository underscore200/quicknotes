
				function microtime() {
				  var now = new Date().getTime() / 1000;
				  return now;
				}

				$(document).ready(function() {
            SyntaxHighlighter.defaults['gutter'] = false;
  					SyntaxHighlighter.all();

  					var i_height = 70;
					var res = [];
  					var sitems = $('#search-items')[0];

  					function toggleHeight(selector) {
  						var $selector = $(selector);
              //$('#search-items a.short-code').removeClass('selected');
              $selector.toggleClass('selected');
  						if($selector.css('max-height') == '70px'){
                $selector.css('max-height', '800px');
                $selector.css('overflow-y', 'scroll');
              }
              else {
                $selector.css('max-height', '70px');
  							$selector.css('overflow-y', 'hidden');
              }
  					}

  					$(document).on('click', '.short-code', function() { toggleHeight(this); });

  					function render() {
  						var count = 1;
  						$('#search-items p.short-code').remove();
  						$(res).each(function() {
  							$('<p>').prop('href', '#' + this.id).prop('class', 'short-code list-group-item ' 
  								+ ((count++ % 2)?'odd-item':'')).
  							append($('<pre>').addClass('brush:' + this.lang).text(this.body))
  							.appendTo($(sitems));
  							SyntaxHighlighter.highlight();
  						});
  					}

  					function queryS(query) {
              query = query.trim();
              if(query.length < 2) {
                return;
              }

  						$.ajax({
  							url: '/notes/qnoteapi',
   							data: {
                   "query":  query,
   							   "format": 'json'
   							},
   							error: function() {
   							   alert('Query Failed');
   							},
   							success: function(data) {
   							   res = data;
   							   render();
   							},
   							type: 'GET'
  						});
  					}

  					var query_maker;
  					$('input#jax-input').on('keyup', function() {
  						$this = this;
  						clearTimeout(query_maker);
  						query_maker = setTimeout(function(){ queryS($this.value); }, 900);
  					});
				});