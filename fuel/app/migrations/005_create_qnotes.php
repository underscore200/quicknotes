<?php

namespace Fuel\Migrations;

class Create_qnotes
{
	public function up()
	{
		\DBUtil::create_table('qnotes', array(
			'id' => array('constraint' => 11, 'type' => 'int', 'auto_increment' => true, 'unsigned' => true),
			'body' => array('type' => 'text'),
			'lang' => array('constraint' => 20, 'type' => 'varchar', 'default' => 'null'),
			'created_at' => array('constraint' => 11, 'type' => 'int', 'null' => true),
			'updated_at' => array('constraint' => 11, 'type' => 'int', 'null' => true),
			'deleted_at' => array('constraint' => 11, 'type' => 'int', 'null' => true),

		), array('id'), false, 'MyISAM', 'utf8_general_ci');
	}

	public function down()
	{
		\DBUtil::drop_table('qnotes');
	}
}