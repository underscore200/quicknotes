<?php

namespace Fuel\Migrations;

class Create_tags
{
	public function up()
	{
		\DBUtil::create_table('tags', array(
			'id' => array('constraint' => 11, 'type' => 'int', 'auto_increment' => true, 'unsigned' => true),
			//'count' => array('constraint' => 11, 'type' => 'int', 'default' => '0'),
			'name' => array('constraint' => 255, 'type' => 'varchar'),
			'created_at' => array('constraint' => 11, 'type' => 'int', 'null' => true),
			'updated_at' => array('constraint' => 11, 'type' => 'int', 'null' => true),

		), array('id'), false, 'MyISAM', 'utf8_general_ci');

		\DBUtil::create_index('tags', 'name', '', 'UNIQUE');
	}

	public function down()
	{
		\DBUtil::drop_table('tags');
	}
}