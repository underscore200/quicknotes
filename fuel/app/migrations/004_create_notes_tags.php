<?php

namespace Fuel\Migrations;

class Create_notes_tags
{
	public function up()
	{
		\DBUtil::create_table('notes_tags', array(
			'note_id' => array('constraint' => 11, 'type' => 'int'),
			'tag_id' => array('constraint' => 11, 'type' => 'int'),
			'created_at' => array('constraint' => 11, 'type' => 'int', 'null' => true),
			'updated_at' => array('constraint' => 11, 'type' => 'int', 'null' => true),

		), array('note_id', 'tag_id'));
	}

	public function down()
	{
		\DBUtil::drop_table('notes_tags');
	}
}