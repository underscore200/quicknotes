<?php

namespace Fuel\Migrations;

class Create_notes
{
	public function up()
	{
		\DBUtil::create_table('notes', array(
			'id' => array('constraint' => 11, 'type' => 'int', 'auto_increment' => true, 'unsigned' => true),
			'title' => array('constraint' => 255, 'type' => 'varchar'),
			'body' => array('type' => 'text'),
			'category_id' => array('constraint' => 11, 'type' => 'int', 'default' => "0"),
			'is_public'  => array('constraint' => '"0","1"', 'type' => 'enum', "default" => "1"),
			'usefulness' => array('constraint' => '"1","2","3","4","5"', 'type' => 'enum', "default" => "2"),
			'views' => array('constraint' => 11, 'type' => 'int', "default" => "0"),
			'author_id' => array('constraint' => 11, 'type' => 'int'),
			'created_at' => array('constraint' => 11, 'type' => 'int', 'null' => true),
			'updated_at' => array('constraint' => 11, 'type' => 'int', 'null' => true),
			'deleted_at' => array('constraint' => 11, 'type' => 'int', 'null' => true),

		), array('id'), false, 'MyISAM', 'utf8_general_ci');
	}

	public function down()
	{
		\DBUtil::drop_table('notes');
	}
}