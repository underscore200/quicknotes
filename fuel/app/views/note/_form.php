<?php echo Form::open(array("class"=>"form-horizontal")); ?>

	<fieldset>
		<div class="form-group">
			<?php echo Form::label('Title', 'title', array('class'=>'control-label')); ?>
				<?php echo Form::input('title', Input::post('title', isset($note) ? $note->title : ''), array('class' => 'form-control editor-input', 'placeholder'=>'Title')); ?>

		</div>
		<div class="form-group">
			<?php echo Form::label('Body', 'body', array('class'=>'control-label')); ?>
				<?php echo Form::textarea('body', htmlentities( Input::post('body', isset($note) ? $note->body : '') ), array('id' => 'editor', 'class' => 'form-control editor-input', 'rows' => 12, 'placeholder'=>'Body')); ?>

		</div>

        <div class='row'>
			<div class='col-sm-5 col-xs-5' style='margin-right:4%; margin-left:4%;'>
				<div class="form-group">
					<?php echo Form::label('Category id', 'category_id', array('class'=>'control-label')); ?>
					<?php echo Form::select('category_id', Input::post('category_id', isset($note) ? $note->category_id : '1'), $categories, array('class' => 'form-control editor-input', 'placeholder'=>'Category id')); ?>

				</div>
			</div>
			<div class='col-sm-5 col-xs-5' style='margin-right:4%; margin-left:4%;'>
				<div class="form-group">
					<?php echo Form::label('Usefulness level: ', 'usefulness', array('class'=>'control-label')); ?>
                    <?php echo Form::select('usefulness', Input::post('usefulness', isset($note) ? $note->usefulness : '3'), $use_array, array('class' => 'form-control editor-input', 'placeholder'=>'Usefulness')); ?>

				</div>
			</div>
			<div class='col-sm-10  col-xs-10' style='margin-right:4%; margin-left:4%;'>
				<div class="form-group">
					<?php echo Form::label('Tag list', 'tag_list', array('class'=>'control-label'));
					$tag_list = '';
					if(isset($note))
					{
						$t = array();
						foreach ($note->tags as $tag)
						{
							$t[] = $tag->name;
						}
						$tag_list = implode(',', $t);
					}
					echo Form::input('tag_list', Input::post('tag_list', isset($note) ? $tag_list : ''), array('id' => 'tags', 'class' => 'form-control editor-input', 'placeholder'=>'Tag list')); ?>

				</div>
			</div>

		</div>

		<div class='row'>
		<div class="form-group">
			<div class="checkbox pull-left">
				<label>
					<?php 
					$is_public = Input::post('is_public', isset($note) ? $note->is_public : '1') === '1';
					echo Form::checkbox('is_public', '0', true, array()); 
					echo Form::checkbox('is_public', '1', $is_public, array()); 
					echo "Is public?";
					?>
				</label>
			</div>
		</div>
		</div>

		<?php echo Form::hidden('author_id', Input::post('author_id', isset($note) ? $note->author_id : '1'), array('class' => 'form-control', 'placeholder'=>'Author id')); ?>
		<div class="form-group">
			<label class='control-label'>&nbsp;</label>
			<?php echo Form::submit('submit', 'Save', array('class' => 'btn btn-primary msub')); ?>		</div>
	</fieldset>
<?php echo Form::close(); ?>