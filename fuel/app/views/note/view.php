<?php
/**
 * @Author: i403
 * @Date:   2015-10-10 11:25:24
 * @Last Modified by:   i403
 * @Last Modified time: 2015-10-10 13:06:43
 */
?>

    <!-- Page Content -->
    <div class="container" style="max-width: 850px;">

        <div class="row">

            <!-- Post Content Column -->
            <div class="col-lg-12">

                <!-- Post -->
				<span class='muted' style="display:none;">#<?php echo $note->id; ?></span>
                <!-- Title -->
                <h2><?php echo $note->title; ?></h2>

                <!-- Author 
                <p class="lead">
                    by <a href="#">Start Bootstrap</a>
                </p>-->

                <hr>

                <!-- Date/Time -->
                <div style="overflow: hidden"><span class="col-lg-10">
                    <span class="glyphicon glyphicon-time"></span> Posted on <abbr title="<?php echo date('Y-m-d h:i:s A P', $note->created_at); ?>">
                	<?php echo date('F dS \a\t h A', $note->created_at); ?></abbr>
                    </span>
                	<h4 class="col-lg-2" style="display: inline; margin-top: 0px" ><a href="<?= Uri::create('note') . '?cid=' . $note->category_id; ?>" class="label label-primary pull-right"><?php echo $categories[$note->category_id]; ?></a></h4>
                </div>

                <hr style="margin: 5px 0px 20px;">

                <!-- Post Content -->
                <div class="content"  style=""><?php echo $note->body; ?></div>

                <hr>


                <div class="col-sm-8 col-md-8 col-lg-8" style="margin-bottom: 30px;">
                    <?php foreach ($note->tags as $tag) : ?>
                        <h5 style="display: inline"><a class="label label-default" href="<?= Uri::create('note') . '?tag=' . $tag->name; ?>"><?php echo $tag->name; ?></a></h5>
                    <?php endforeach; ?>
                </div>
                <span class="pull-right col-sm-4 col-md-4 col-lg-4 text-right" >
                    <?php echo Html::anchor('note/edit/'.$note->id, '<i class="glyphicon glyphicon-wrench"></i> Edit', array('class' => 'btn btn-primary btn-sm')); ?>
                    <?php echo Html::anchor('note/delete/'.$note->id, '<i class="glyphicon glyphicon-trash"></i> Delete', array('class' => 'btn btn-sm btn-danger', 'onclick' => "return confirm('Are you sure?')")); ?>
                    <?php echo Html::anchor('note', '<i class="glyphicon glyphicon-th-list"></i> Index', array('class' => 'btn btn-default btn-sm')); ?>
				</span>

				<div class='col-sm-9 col-md-7 col-lg-6'>
				<div> Importance</div>
				<div class="progress">
  					<div class="progress-bar" role="progressbar" aria-valuenow="<?php echo $note->usefulness * 20;?>"
  					aria-valuemin="0" aria-valuemax="100" style="width: <?php echo $note->usefulness * 20;?>%;
                        background-color: <?php echo Model_Note::level_to_color($note->usefulness);?>">
  						<?php echo $use_array[$note->usefulness]; ?>
  					</div>
				</div>
				</div>

				<div class="text-muted disabled pull-right text-right col-sm-12 col-md-12 col-lg-12" style="">
					<h3><?php echo $note->views; ?></h3>
				</div>

            </div>

            <!-- Sidebar Widgets Column 
            <div class="col-md-3">

                Categories Well 
                <div class="well col-sm-12">
                    <h4>Categories</h4>
                    <div class="row">
                        <div class="col-lg-12">
                            <ul class="list-unstyled">
                                <li><a href="#">Category Name</a>
                                </li>
                                <li><a href="#">Category Name</a>
                                </li>
                                <li><a href="#">Category Name</a>
                                </li>
                                <li><a href="#">Category Name</a>
                                </li>
                            </ul>
                        </div>
                    </div> 
                </div> -->

                <!-- Side Widget Well 
                <div class="well">
                    <h4>Side Widget Well</h4>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Inventore, perspiciatis adipisci accusamus laudantium odit aliquam repellat tempore quos aspernatur vero.</p>
                </div>-->

            </div>

        </div>
        <!-- /.row -->


    </div>
    <!-- /.container -->