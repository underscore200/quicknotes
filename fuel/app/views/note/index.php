<div class="container" style="max-width: 850px;">
    <?php if ($notes): ?>
        <?php foreach ($notes as $note): ?>

            <div class="row">

                <!-- Post Content Column -->
                <div class="col-lg-12">

                    <!-- Post -->
                    <span class='muted' style="display:none;">#<?php echo $note->id; ?></span>
                    <!-- Title -->
                    <h3><?php echo Html::anchor('note/view/'.$note->id, $note->title, array()); ?></h3>

                    <!-- Date/Time -->
                    <p>
                        Posted on <abbr title="<?php echo date('Y-m-d h:i:s A P', $note->created_at); ?>">
                            <?php echo date('F dS \a\t h A', $note->created_at); ?></abbr>
                        <a href="<?= Uri::create('note') . '?cid=' . $note->category_id; ?>" class="label label-primary pull-right"><?php echo $categories[$note->category_id]; ?></a>
                    </p>

                    <!-- Post Content -->
                    <div class="content"  style=""><?php echo AppSecurity::shorten_html($note->body); ?></div>

                    <div class="col-sm-6 col-md-6 col-lg-6" style="margin-bottom: 30px;">
                        <?php foreach ($note->tags as $tag) : ?>
                            <h5 style="display: inline-block"><a href="<?= Uri::create('note') . '?tag=' . $tag->name; ?>" class="label label-default"><?php echo $tag->name; ?></a></h5>
                        <?php endforeach; ?>
                    </div>

                    <div class='col-sm-6 col-md-6 col-lg-6' style="margin-top: 10px;">
                        <div class="progress">
                            <div class="progress-bar" role="progressbar" aria-valuenow="<?php echo $note->usefulness * 20;?>"
                                 aria-valuemin="0" aria-valuemax="100" style="width: <?php echo $note->usefulness * 20;?>%;
                                background-color: <?php echo Model_Note::level_to_color($note->usefulness);?>">
                                <?php echo $use_array[$note->usefulness]; ?>
                            </div>
                        </div>
                    </div>

                </div>

            </div>
            <hr>
            <!-- /.row -->


        <?php endforeach; ?>

    <?php else: ?>
        <p>No Notes.</p>

    <?php endif; ?>
    <nav>
        <ul class="pager">
            <li class="previous <?php echo ($page < 2) ? 'disabled' : ''; ?>">
                <?php echo ($page < 2) ? '' : Html::anchor(Uri::update_query_string(array('page' => $page - 1)), '<span aria-hidden="true">&larr;</span> Previous'); ?>
            </li>
            <li class="next <?php echo ($page >= $maxpage) ? 'disabled' : ''; ?>">
                <?php echo ($page >= $maxpage) ? '' : Html::anchor(Uri::update_query_string(array('page' => $page + 1)), 'Next <span aria-hidden="true">&rarr;</span>'); ?>
            </li>
        </ul>
    </nav>
</div>