<h2>New <span class='muted'>Note</span></h2>
<br>

<?php echo render('note/_form'); ?>


<p><?php echo Html::anchor('note', '<i class="glyphicon glyphicon-th-list"></i> Index', array('class' => 'btn btn-default btn-sm pull-right')); ?></p>
