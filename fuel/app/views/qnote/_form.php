<?php echo Form::open(array("class"=>"form-horizontal")); ?>

	<fieldset>
		<div class="form-group">
			<?php echo Form::label('Body', 'body', array('class'=>'control-label')); ?>

				<?php echo Form::textarea('body', Input::post('body', isset($qnote) ? $qnote->body : ''), array('class' => 'col-md-8 form-control editarea', 'rows' => 8, 'placeholder'=>'Body')); ?>

		</div>
		<div class="form-group">
			<?php echo Form::label('Language', 'lang', array('class'=>'control-label')); ?>

				<?php echo Form::select('lang', Input::post('lang', isset($qnote) ? $qnote->lang : 'plain'), Model_Qnote::LANGS, array('class' => 'form-control')); ?>

		</div>
		<div class="form-group">
			<label class='control-label'>&nbsp;</label>
			<?php echo Form::button('cnewlines', '<i class="glyphicon glyphicon-filter"></i>Clear double newlines', array('class' => 'btn btn-default', 'id' => 'cnewlines')); ?>	
			<?php echo Form::submit('submit', 'Save', array('class' => 'btn btn-primary')); ?>		</div>
	</fieldset>
<?php echo Form::close(); ?>