<?php if ($qnotes): ?>
<div class="container qnote-list">
	<div class="list-group" id='search-items'>
		<p class="list-group-item search-item">
			<a class="top-home" href="<?= Uri::create("qnote"); ?>"><i class="glyphicon glyphicon-home"></i></a>
			<?= Form::input('search', '', array('id' => 'jax-input', 'class' => 'jax-input', 'autofocus', 'autocomplete' => 'off')); ?>
			<i class="glyphicon glyphicon-search" style="position: relative; left: -30px; top: 4px;"></i>
		</p>
	</div>
	<p class="pull-right" style="wdith:600px;">
	<?php echo Html::anchor('qnote/create', '<i class="glyphicon glyphicon-plus"></i> Add', array('class' => 'btn btn-default add-btn')); ?>
</p>
</div>
<?php else: ?>
<p>No Qnotes.</p>

<?php endif; ?>
