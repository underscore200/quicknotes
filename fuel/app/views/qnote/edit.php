<h2>Editing <span class='muted'>Qnote</span></h2>
<br>

<?php echo render('qnote/_form'); ?>
<p>
	<?php echo Html::anchor('qnote/view/'.$qnote->id, 'View'); ?> |
	<?php echo Html::anchor('qnote', 'Back'); ?></p>
<script type="text/javascript">
	$(document).ready(function() {
		$('#cnewlines').click(function(e) {
			e.preventDefault();
			text = $('textarea.editarea').val();
			$('textarea.editarea').val(text.replace(/\n\s*\n/g, '\n'));
		});
	});
</script>
