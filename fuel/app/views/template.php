<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title><?php echo $title; ?></title>
	<link rel="icon" href="/notes/assets/img/notes.ico" type="image/x-icon" />
	<?php echo Asset::css($mode.'bootstrap.min.css'); ?>
    <?php echo Asset::css('font-awesome.min.css'); ?>
    <?php echo Asset::css($mode.'notes.css'); ?>
    <?php if(isset($editor)) : ?>
        <?php Asset::add_path('assets/css/smoothness', 'css'); ?>
        <?php echo Asset::css('summernote.css'); ?>
        <?php echo Asset::css('jquery.tagsinput.min.css'); ?>
        <?php echo Asset::css('jquery-ui.min.css'); ?>


    <?php elseif(isset($viewer)) : ?>

		<?php Asset::add_path('assets/css/shHighlight', 'css'); ?>
		<?php Asset::add_path('assets/js/shHighlight', 'js'); ?>
		<?php echo Asset::css('shCore.css'); ?>
		<?php echo Asset::css($mode.'shThemeDefault.css'); ?>

		<?php echo Asset::js('shCore.js'); ?>
		<?php echo Asset::js('shBrushPhp.js'); ?>
		<?php echo Asset::js('shBrushJScript.js'); ?>
		<?php echo Asset::js('shBrushBash.js'); ?>
		<?php echo Asset::js('shBrushSql.js'); ?>
	<?php endif; ?>

   <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

	<style>
		body { margin: 40px; }
        .syntaxhighlighter {
            overflow-y: hidden !important;
            overflow-x: auto !important;
            padding: 5px 15px;
            border-radius: 5px;
            border: 1px solid #888;
        }
	</style>
</head>
<body>
	<div class="container" style="max-width: 850px;">
        <?php if(!isset($page_category)) echo render('common/header'); ?>
		<div class="col-md-12">
<?php if (Session::get_flash('success')): ?>
			<div class="alert alert-success">
				<strong>Success</strong>
				<p><?php echo implode('</p><p>', e((array) Session::get_flash('success'))); ?></p>
			</div>
<?php endif; ?>
<?php if (Session::get_flash('error')): ?>
			<div class="alert alert-danger">
				<strong>Error</strong>
				<p><?php echo implode('</p><p>', e((array) Session::get_flash('error'))); ?></p>
			</div>
<?php endif; ?>
<?php if (Session::get_flash('warning')): ?>
			<div class="alert alert-warning">
				<strong>Hmm..</strong>
				<p><?php echo implode('</p><p>', e((array) Session::get_flash('warning'))); ?></p>
			</div>
<?php endif; ?>
		</div>
		<div class="col-md-12">
<?php echo $content; ?>
		</div>
		<footer style="max-width: 850px;">
		<?php echo Asset::js('jquery-1.11.3.min.js'); ?>
        <?php if(isset($editor)) echo Asset::js('jquery-ui.min.js'); ?>
		<?php echo Asset::js('bootstrap.min.js'); ?>
		<?php if(isset($editor)) : ?>
			<?php echo Asset::js('summernote.min.js'); ?>
			<?php echo Asset::js('jquery.tagsinput.min.js'); ?>
			<script>
				$(document).ready(function() {
					$('#tags').tagsInput({
                        autocomplete_url:'<?= Uri::create('tag/view'); ?>',
						'height':'50px',
   						'width':'auto'
					});

					// begin editor
  					$('#editor').summernote({
        				height: 200,
        				airMode: false
      				});
				});
			</script>
		<?php elseif(isset($viewer)) : ?>
			<script>
				$(document).ready(function() {
                    SyntaxHighlighter.defaults['gutter'] = false;
  					SyntaxHighlighter.all();
				});
			</script>
		<?php endif; ?>

            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <p class="text-left">Copyrighted &copy; <b>Khatcher Holmes</b></p>
                </div>
            </div>
			<p class="pull-right">Page rendered in {exec_time}s using {mem_usage}mb of memory.</p>
			<p>
				Built using FuelPHP. <a href="http://fuelphp.com">FuelPHP</a> is released under the MIT license.<br>
				<small>Version: <?php echo e(Fuel::VERSION); ?></small>
			</p> 
		</footer>
	</div>
</body>
</html>
