<?php
/**
 * Created by i403
 * Date: 10/11/15 5:10 PM
 */

?>
<!-- Fixed navbar -->
<nav class="navbar navbar-default navbar-fixed-top">
    <div class="container" style="max-width: 1000px;">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="<?php echo Uri::base(); ?>" style="padding: 5px 25px;">
                <?php echo Asset::img('notes.png', array('alt' => 'Top logo')); ?>
            </a>
            <?php echo Html::anchor('note', 'Top', array('class' => 'navbar-brand')); ?>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav navbar-right">
                <li><p class="navbar-btn"><?php echo Html::anchor('note/create', 'Add new Note', array('class' => 'btn btn-success')); ?></p></li>
            </ul>
            <div class="col-sm-8 col-md-6 pull-right">
                <form class="navbar-form form-inline" role="search" action="/notes/note">
                    <div class="form-group">
                        <select class="form-control" name="order_by">
                            <?php
                            (!isset($s_o)) and $s_o = 1;
                            foreach ($orders as $key=>$val) :?>
                                <option <?= (2*$key - 1 == $s_o) ? 'selected' : ''; ?> value="<?= 2*($key - 1) + 1; ?>">+<?= $val; ?></option>
                                <option <?= (2*$key     == $s_o) ? 'selected' : ''; ?> value="<?= 2*$key;           ?>">-<?= $val; ?></option>
                            <?php endforeach ;?>
                        </select>
                    </div>
                    <div class="input-group" style="max-width: 250px">
						<?php echo Form::input('q', Input::get('q', null), array('class' => 'form-control', 'placeholder'=>'Search', 'autocomplete' => 'off', 'size' => 'on')); ?>
                        <div class="input-group-btn">
                            <button class="btn btn-default" type="submit"><i class="glyphicon glyphicon-search"></i></button>
                        </div>
                    </div>
                </form>
            </div>
        </div><!--/.nav-collapse -->
    </div>
</nav>