<?php 
#seeding data

namespace Fuel\Tasks;

class Seedqnotes
{
	public static function run()
	{
		$qnotes = array(
			'php' => array("print array\nprint_r(\$arr);",
					"dump a variable\nvar_dump(\$var);",
					"die after echoing\n echo \$msg; die;\nor exit(\$msg);\n or die(\$msg);"),
			'javascript' => array("// define variable\nva a = 'b';"),
			'bash'       => array("Bash Export Command Example\nexport varname=value",
					"$ export country=India\n$ env\nSESSIONNAME=Console\ncountry=India\n_=/usr/bin/env")
		);

		echo "Saving Qnotes\n";
		foreach ($qnotes as $lang => $arr) {
			foreach ($arr as $value) {
				$p = \Model_Qnote::forge();
				$p->lang = $lang;
				$p->body = $value;
				$p->save();

			}
		}
		echo "Seeding done!\n";
	}

	# Reset all records in certain model, eg:
	#	php oil r seed:reset 'project';
	# note the model name using a singularized name!
	public static function reset($modelname='')
	{
		if ($modelname)
		{
			# Since models are singularized name,
			# the model name must be pluralized
			\DBUtil::truncate_table(\Inflector::pluralize($modelname));

			echo "All records on model $modelname successfully reset";
		}
		else {
			// I dont know how to list all models so I can directly
			// reset all tables...
			// So you must specify the model name instead like
			// example above... :(
			echo "Please specify a model name!";
		}
	}
}