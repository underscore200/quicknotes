<?php 
#seeding data

namespace Fuel\Tasks;

class Seedtags
{
	public static function run()
	{
		$tags = array(
			'php', 'c', 'c++', 'ruby', 'mysql', 'python', 'trick', 'cheatsheet', 'css', 'html',
			'go', 'google', 'frameworks', 'wireless', 'photoshop', 'gimp', 'liverpool', 'premier league',
			'rakuten', 'apple', 'goodlinks', 'WW2', 'WW1', 'Fallout', 'whatif', 'download', 'mustread',
			'testing', 'mystery', 'solve', 'funny', 'thinkaboutit', 'idontthink', 'hoooly', 'suuure',
			'willyoujust', 'politics', 'money', '日本語', 'まとめ', 'git', 'development', 'devflow', 'DIY',
			'ideas', 'smartphone', 'apps', 'android', 'ios', 'java', 'javascript', 'fuelphp', 'laravel', 'cakephp',
			'coding technique'
		);

		echo "Saving Tags\n";
		foreach ($tags as $name) {
			$p = \Model_Tag::forge();
			$p->name = $name;
			$p->save();
		}
		echo "Seeding done!\n";
	}

	# Reset all records in certain model, eg:
	#	php oil r seed:reset 'project';
	# note the model name using a singularized name!
	public static function reset($modelname='')
	{
		if ($modelname)
		{
			# Since models are singularized name,
			# the model name must be pluralized
			\DBUtil::truncate_table(\Inflector::pluralize($modelname));

			echo "All records on model $modelname successfully reset";
		}
		else {
			// I dont know how to list all models so I can directly
			// reset all tables...
			// So you must specify the model name instead like
			// example above... :(
			echo "Please specify a model name!";
		}
	}
}