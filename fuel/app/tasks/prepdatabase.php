<?php 
#seeding data

namespace Fuel\Tasks;

class Prepdatabase
{
	public static function run()
	{
		echo "Creating 'tools_dev' database\n";
		\DBUtil::create_database("tools_dev", 'utf8', true);
		echo "Done!\n";
	}

	public static function reset()
	{
		echo "Dropping database\n";
		\DBUtil::drop_database('tools_dev');
		echo "Done!\n";
	}
}