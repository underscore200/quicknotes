<?php 
#seeding data

namespace Fuel\Tasks;

class Seedcategories
{
	public static function run()
	{
		$categories = array(
			"General"           => "General topics that doesn;t belong to only one category",
			"Tips & Tricks"     => "Howtos and other practices for things such as commandline",
			"Programming"       => "Best practices or things to learn",
			"Software design"   => "Architecture or flow of software design",
			"Visual Design"      => "Best practices",
			"Work related"	    => "Notes related to current work or anything",
			"Company"		    => "Notes related to company",
			"IT General"		=> "General things about IT industry and companies",
			"Football or Sports" => "All things with football, transfers, other sports, epic videos",
			"Lifestyle" 		=> "Notes about 'how to live'.. Haha, just kidding. Another tips and howtos",
			"Opinion & Thought"	=> "Thoughts or opinions about anything. Practices, general life, ...",
			"Puzzles & Mysteries" => "Math puzzles or physics mysteries kinda thing",
			"History"	 		=> "All stuff about history (history of anything)",
			"Movies & Books"	=> "All stuff about history (history of anything)",
			"Economics" 		=> "Anything related to economics",
			"Finance" 			=> "Learned stuff from finance",
			"Innovations"		=> "Ideas and innovations",
			"Games & Video" 	=> "Games, releases, videos and such",
			"Travel & Photos" 	=> "Places to visit, interesting sopts in the world",
			"Random"		    => "Random sentences or words, or even characters."
		);

		foreach ($categories as $title => $description) {
			echo "Saving $title\n";
			$p = \Model_Category::forge();
			$p->name = $title;
			$p->description = $description;
			$p->save();
		}
		echo "\nSeeding done!\n";
	}

	# Reset all records in certain model, eg:
	#	php oil r seed:reset 'project';
	# note the model name using a singularized name!
	public static function reset($modelname='')
	{
		if ($modelname)
		{
			# Since models are singularized name,
			# the model name must be pluralized
			\DBUtil::truncate_table(\Inflector::pluralize($modelname));

			echo "All records on model $modelname successfully reset";
		}
		else {
			// I dont know how to list all models so I can directly
			// reset all tables...
			// So you must specify the model name instead like
			// example above... :(
			echo "Please specify a model name!";
		}
	}
}