<?php

class Controller_Note extends Controller_Base
{

	public function before()
	{
		parent::before();

		$category_models = Model_Category::find('all', array('limit' => '100'));
		$categories = array();
		foreach ($category_models as $key => $value)
		{
			$categories[$value->id] = $value->name;
		}

		$this->template->set_global('categories', $categories, false);
		$this->template->set_global('orders', Model_Note::ORDERBY_ARRAY, false);
		$this->template->set_global('use_array', Model_Note::USEFULNESS_ARRAY, false);
	}

	public function action_index()
	{
		// get page
		$page = Input::get('page', 1);
		$q = Input::get('q', null);
		$o = Input::get('order_by', 2);
		$c = Input::get('cid', null);
		$t = Input::get('tag', null);

		(!is_numeric($page) || $page < 0) and $page = 1;
		$page = intval($page);
		$pp = 6;

		(!is_numeric($o) || intval($o) < 1 || intval($o) > 10) and $o = 2;

		$val = Validation::forge('index');
		$val->add_field('p', 'page',         'valid_string[numeric]|numeric_min[1]');
		$val->add_field('q', 'search query', 'min_length[3]|max_length[100]');
		$val->add_field('o', 'order id',     'required|valid_string[numeric]|numeric_between[1,10]');
		$val->add_field('c', 'category id',  'valid_string[numeric]');
		$val->add_field('t', 'tag name',     'max_length[100]|valid_string[alpha,numeric,spaces,dots]');

		// TODO: finish up the fuelPHP validation

		$da = ($o % 2 === 1);
		$order_col = (int)(($o - 1) / 2 + 1);
		$a = (Model_Note::ORDERBY_ARRAY);
		$this->template->set_global('s_o',     $o, false);

		// check if this is a query search
		// otherwise check if asked to be list by category
		// or tag search
		if (!is_null($q))
		{
			if(mb_strlen($q) > 3 && mb_strlen($q) < 100 && strpos($q, '\\') === false)
			{
				$arr = Model_Note::search($q, ($page - 1) * $pp, $pp, $a[$order_col], $da);
				$count = $arr['count'];
				$data['notes'] = $arr['notes'];
				$data['page']    = $page;
				$data['maxpage'] = $this->get_maxpage($count, $pp);
				$this->template->title   = "Notes | Search for '$q'";
				$this->template->content = View::forge('note/index', $data);

				return;
			}
			else
			{
				Session::set_flash('warning', 'Search query too damn short or too damn long!'.
					"\n\t\t".'Or it just might be containing illegal character like.., I don\'t know "\"');
			}
		}
		elseif (!is_null($c) && is_numeric($c) && intval($c) > 0)
		{
			$count = Model_Note::count(array('where' => array( array( 'category_id', '=', $c ))));
			$data['notes'] = Model_Note::find('all', array(
				'where' => array(array('category_id', '=', $c)),
				'limit' => $pp,
				'offset' => ($page - 1) * $pp
			));
			$data['page'] = $page;
			$data['maxpage'] = $this->get_maxpage($count, $pp);
			$this->template->title = "Notes | Search by category";
			$this->template->content = View::forge('note/index', $data);

			return;
		}
		elseif (!is_null($t) && mb_strlen($t) > 0)
		{
			$t = $this->like($t, '\\');
			$tag = Model_Tag::find('first', array(
				'where' => array(array('name', '=', $t)),
			));
			if($tag)
			{
				$notes = Model_Note::find('all', array(
					'related' => array(
						'tags' => array(
							'where' => array(
								array('id', '=', $tag->id)
							)
						)
					),
				//	'row_limit' => $pp,					// Limiting when using related gives complex results (mostly empty)
				//	'row_offset' => ($page - 1) * $pp
				));
				$data['notes'] = $notes;
				$data['page'] = 1; //$page;
				$data['maxpage'] = 1; //$this->get_maxpage($count, $pp);
				$this->template->title = "Notes | Search by category";
				$this->template->content = View::forge('note/index', $data);

				return;
			} else {
				Session::set_flash('warning', "tag '$t' doesn't exist... Or does it?... Dum dum dumm...");
			}

		}

		$data['notes'] = Model_Note::find('all', array(
			'limit' => $pp,
			'offset' => ($page - 1) * $pp,
			'order_by' => array($a[$order_col] => ($da) ? 'ASC' : 'DESC')
		));
		$data['page'] = $page;
		$data['maxpage'] = (int)((Model_Note::count() - 1) / $pp + 1);
		$this->template->title = "Notes | Latest";
		$this->template->content = View::forge('note/index', $data);

	}

	public function action_view($id = null)
	{
		is_null($id) and Response::redirect('note');

		if (!$nt = Model_Note::find($id))
		{
			Session::set_flash('error', 'Could not find note with id:' . $id);
			Response::redirect('note');
		}

		$data['note'] = $nt;
		Model_Note::increment_view($id);
		$this->template->viewer = true;
		$this->template->title = "Notes | " . $nt->title;
		$this->template->content = View::forge('note/view', $data);

	}

	public function action_create()
	{
		if (Input::method() == 'POST')
		{
			$val = Model_Note::validate('create');

			if ($val->run())
			{
				$note = Model_Note::forge(array(
					'title' => Input::post('title'),
					'body' => Input::post('body'),
					'category_id' => Input::post('category_id'),
					'is_public' => Input::post('is_public'),
					'usefulness' => Input::post('usefulness'),
					'views' => 0,
					'author_id' => Input::post('author_id'), // TODO: change this to session user_id
				));

				$tags = explode(',', Input::post('tag_list'));
				foreach ($tags as $tagname)
				{
					$note->tags[] = Model_Tag::find_or_create($tagname);
				}
				if ($note and $note->save())
				{
					//$this->save_tags(Input::post('tag_list'), $note->id);

					Session::set_flash('success', 'Added note "' . $note->title . '"');
					Response::redirect('note/view/' . $note->id);
				}

				else
				{
					Session::set_flash('error', 'Could not save note.');
				}
			}
			else
			{
				Session::set_flash('error', $val->error());
			}
		}

		$this->template->editor = true;
		$this->template->title = "Notes | Add new note";
		$this->template->content = View::forge('note/create');

	}

	public function action_edit($id = null)
	{
		is_null($id) and Response::redirect('note');

		if (!$note = Model_Note::find($id))
		{
			Session::set_flash('error', 'Could not find note with id: ' . $id);
			Response::redirect('note');
		}

		$val = Model_Note::validate('edit');

		if ($val->run())
		{
			$note->title = Input::post('title');
			$note->body = Input::post('body');
			$note->category_id = Input::post('category_id');
			// tags should not be editable, otherwise they get incremented on every post edit
			//$note->tag_list = Input::post('tag_list');
			$note->is_public = Input::post('is_public');
			$note->usefulness = Input::post('usefulness');
			$note->author_id = Input::post('author_id');

			$tags = explode(',', Input::post('tag_list'));
			foreach ($tags as $tagname)
			{
				$note->tags[] = Model_Tag::find_or_create($tagname);
			}

			if ($note->save())
			{
				Session::set_flash('success', 'Updated note "' . $note->title . '"');
				Response::redirect('note/view/' . $note->id);
			}

			else
			{
				Session::set_flash('error', 'Could not update note "' . $note->title . '""');
			}
		}

		else
		{
			if (Input::method() == 'POST')
			{
				$note->title = $val->validated('title');
				$note->body = $val->validated('body');
				$note->category_id = $val->validated('category_id');
				$note->tag_list = $val->validated('tag_list');
				$note->is_public = $val->validated('is_public');
				$note->usefulness = $val->validated('usefulness');
				// $note->views = $val->validated('views');
				$note->author_id = $val->validated('author_id');

				Session::set_flash('error', $val->error());
			}

			$this->template->set_global('note', $note, false);
		}

		$this->template->editor = true;
		$this->template->title = "Notes | Editing note: " . $note->title;
		$this->template->content = View::forge('note/edit');

	}

	public function action_delete($id = null)
	{
		is_null($id) and Response::redirect('note');

		if ($note = Model_Note::find($id))
		{
			$title = $note->title;
			$note->delete();

			Session::set_flash('success', 'Deleted note "' . $title . '""');
		}

		else
		{
			Session::set_flash('error', 'Could not delete note #' . $id);
		}

		Response::redirect('note');

	}

	private function save_tags($tag_list, $note_id)
	{
		if (empty($tag_list))
		{
			return;
		}

		$tags = explode(',', $tag_list);
		foreach ($tags as $tagname)
		{
			Model_Tag::create_or_update($tagname, $note_id);
		}
	}

	private function get_maxpage($count, $per_page)
	{
		return (int)(($count - 1) / $per_page + 1);
	}

}
