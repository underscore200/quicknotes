<?php
/**
 * Created by i403
 * Date: 10/11/15 12:46 PM
 */

class Controller_Base extends Controller_Template
{
    public function before()
    {
        parent::before();

        // define night mode
        $this->template->mode = '';
        if(!Config::get('auto_night_mode'))
        {
            return;
        }

        $hour   = 18;
        $c_hour = (int) date('H');
        $minute = 0;
        $c_min  = (int) date('i');
        $cnf = Config::get('auto_night_mode_time');
        if(isset($cnf))
        {
            $arr = date_parse_from_format( "H:i", Config::get('auto_night_mode_time'));
            if(!empty($arr) && $arr['warning_count'] < 1 && $arr['error_count'] < 1)
            {
                $hour   = $arr['hour'];
                $minute = $arr['minute'];
            }
        }

        ($hour < 6) and $hour += 24;
        ($c_hour < 6) and $c_hour += 24;
        if($this->is_between($c_hour * 60 + $c_min, $hour * 60 + $minute, 30 * 60))
        {
            $this->template->mode = 'night';
        }
    }

    private function is_between($t, $a, $b)
    {
        return ($t >= $a && $t <= $b);
    }
}