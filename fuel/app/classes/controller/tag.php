<?php

/**
 * note tag controller
 * This should be allowed to edited by UI
 * Therefore remove action_{create,delete,edit}
 * */
class Controller_Tag extends Controller_Rest
{
	public function action_view()
	{
        $name = Input::get('term');
		if(is_null($name) || strlen($name) < 1 )
		{
            $this->format = 'json';
            $this->response(array('msg' => 'argument too short'), 400);
            return;
		}

        if(!Input::is_ajax()) {
            $this->format = 'json';
            $this->response(array('msg' => 'unsupported request'), 400);
            return;
        }

		$tags = Model_Tag::find('all', array('where' => array(array('name', 'LIKE', $name.'%')), 'limit' => '6'));
		if( ! $tags )
		{
			//Response::redirect('tag');
            $this->response(array());
            return;
		}

        $res = array();
        foreach ($tags as $tag) {
            $res[] = array('id' => $tag->id, 'value' => $tag->name);
        }


        //$this->rest_format = 'json';
        $this->response($res);
	}

}
