<?php
/**
 * @Author: i403
 * @Date:   2015-10-18 11:44:09
 * @Last Modified by:   i403
 * @Last Modified time: 2015-10-18 21:41:04
 */

class Controller_Qnoteapi extends Controller_Rest
{

	protected $default_format = 'json';
	
	public function action_index()
	{
		if(!Input::is_ajax())
		{
			//$this->response(array('Unsopported method'), 405);
			//return;
		}

		$query = Input::get('query', null);
		$query = trim($query);
		$res = array();
		if(is_null($query) || $query === '' || mb_strlen($query) < 2) 
		{
			$qnotes = Model_Qnote::find('all');
		} 
		else if (mb_strlen($query) < 4 && mb_strlen($query) >= 2)
		{
			$qnotes = Model_Qnote::search($query, 0, 30, true);
		}
		else
		{
			$qnotes = Model_Qnote::search('+' . $query, 0, 30);
			if(count($qnotes) < 1)
			{
				$qnotes = Model_Qnote::find('all');
			}
		}

       	foreach ($qnotes as $qnote) {
       	    $res[] = array('id' => $qnote->id, 'body' => $qnote->body, 
       	    	'lang' => Model_Qnote::LANGS[$qnote->lang]);
       	}
	
		$this->response($res);
	}
}
