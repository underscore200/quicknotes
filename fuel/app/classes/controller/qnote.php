<?php
class Controller_Qnote extends Controller_Template
{

	public $template = 'qtemplate';
	public function action_index()
	{
		$data['qnotes'] = Model_Qnote::find('all');
		$this->template->title = "Qnotes";
		$this->template->content = View::forge('qnote/index', $data);

	}

	public function action_view($id = null)
	{
		is_null($id) and Response::redirect('qnote');

		if ( ! $data['qnote'] = Model_Qnote::find($id))
		{
			Session::set_flash('error', 'Could not find qnote #'.$id);
			Response::redirect('qnote');
		}

		$this->template->title = "Qnote";
		$this->template->content = View::forge('qnote/view', $data);

	}

	public function action_create()
	{
		if (Input::method() == 'POST')
		{
			$val = Model_Qnote::validate('create');

			if ($val->run())
			{
				$qnote = Model_Qnote::forge(array(
					'body' => Input::post('body'),
					'lang' => Input::post('lang'),
				));

				if ($qnote and $qnote->save())
				{
					Session::set_flash('success', 'Added qnote #'.$qnote->id.'.');

					Response::redirect('qnote');
				}

				else
				{
					Session::set_flash('error', 'Could not save qnote.');
				}
			}
			else
			{
				Session::set_flash('error', $val->error());
			}
		}

		$this->template->title = "Qnotes";
		$this->template->content = View::forge('qnote/create');

	}

	public function action_edit($id = null)
	{
		is_null($id) and Response::redirect('qnote');

		if ( ! $qnote = Model_Qnote::find($id))
		{
			Session::set_flash('error', 'Could not find qnote #'.$id);
			Response::redirect('qnote');
		}

		$val = Model_Qnote::validate('edit');

		if ($val->run())
		{
			$qnote->body = Input::post('body');
			$qnote->lang = Input::post('lang');

			if ($qnote->save())
			{
				Session::set_flash('success', 'Updated qnote #' . $id);

				Response::redirect('qnote');
			}

			else
			{
				Session::set_flash('error', 'Could not update qnote #' . $id);
			}
		}

		else
		{
			if (Input::method() == 'POST')
			{
				$qnote->body = $val->validated('body');
				$qnote->lang = $val->validated('lang');

				Session::set_flash('error', $val->error());
			}

			$this->template->set_global('qnote', $qnote, false);
		}

		$this->template->title = "Qnotes";
		$this->template->content = View::forge('qnote/edit');

	}

	public function action_delete($id = null)
	{
		is_null($id) and Response::redirect('qnote');

		if ($qnote = Model_Qnote::find($id))
		{
			$qnote->delete();

			Session::set_flash('success', 'Deleted qnote #'.$id);
		}

		else
		{
			Session::set_flash('error', 'Could not delete qnote #'.$id);
		}

		Response::redirect('qnote');

	}

}
