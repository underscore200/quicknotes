<?php
use Orm\Model_Soft;

class Model_Note extends Model_Soft
{
	public static $table = 'notes';

	const USEFULNESS_ARRAY = array(
		1 => 'Trivial',
		2 => 'Low',
		3 => 'Average',
		4 => 'Awesome',
		5 => 'Flawless',
	);

    const ORDERBY_ARRAY = array(
    	1 => 'updated_at',
    	2 => 'title',
    	3 => 'category_id',
    	4 => 'views',
    	5 => 'usefulness',
    );

	protected static $_properties = array(
		'id',
		'title',
		'body',
		'category_id',
		//'tag_list',
		'is_public',
		'usefulness',
		'views',
		'author_id',
		'created_at',
		'updated_at',
		'deleted_at',
	);

	protected static $_observers = array(
		'Orm\Observer_CreatedAt' => array(
			'events' => array('before_insert'),
			'mysql_timestamp' => false,
		),
		'Orm\Observer_UpdatedAt' => array(
			'events' => array('before_save'),
			'mysql_timestamp' => false,
		),
	);

	protected static $_soft_delete = array(
        'mysql_timestamp' => false,
    );

	protected static $_many_many = array('tags');

	public static function validate($factory)
	{
		$val = Validation::forge($factory);
		$val->add_field('title', 'Title', 'required|max_length[255]');
		$val->add_field('body', 'Body', 'required|min_length[20]');
		$val->add_field('category_id', 'Category Id', 'required|valid_string[numeric]');
		$val->add_field('tag_list', 'Tag List', 'valid_string[alpha,commas,numeric,spaces,dots]');
		$val->add_field('is_public', 'Is Public', 'valid_string[numeric]');
		$val->add_field('usefulness', 'Usefulness', 'required|valid_string[numeric]|numeric_between[1,5]');
		//$val->add_field('views', 'Views', 'required|valid_string[numeric]');
		$val->add_field('author_id', 'Author Id', 'required|valid_string[numeric]');

		return $val;
	}

	public static function increment_view($id) 
	{
		$note = Model_Note::find($id);
		$note->views++;
		$note->save();
	}

    public static function level_to_color($level, $max_level = 5, $base_value = 160, $base_color = 'blue') {
        $red = ($base_color === 'red')   ? $base_value : (int) (($max_level - $level)*($base_value/($max_level - 1)));
        $gre = ($base_color === 'green') ? $base_value : (int) (($max_level - $level)*($base_value/($max_level - 1)));
        $blu = ($base_color === 'blue')  ? $base_value : (int) (($max_level - $level)*($base_value/($max_level - 1)));
        return "rgb($red, $gre, $blu)";
    }

	/**
	 * @param string $q Query to search for
	 * @param int    $offset Offset (for pagination)
	 * @param int    $limit  Limit
	 * @param string $order_col column to order by
	 * @param bool   $da   DESC or ASC
	 * @param bool   $like Whether or not to use LIKE of mysql (otherwise MATCH AGAINST will be used)
	 * @return array Array in format {'count'=>int, 'notes'=>array}
	 */
	public static function search($q = '', $offset = 0, $limit = 0, $order_col,
		$da = true, $like = false)
	{
		if(is_null($q) || $q === '')
		{
			return array('count' => 0, 'notes' => array());
		} else {
			$q = ($like) ? Model_Note::escape_like($q, '\\') : Model_Note::escape_match($q, '\\');
			if(!$q || $q === '')
			{
				return array('count' => 0, 'notes' => array());
			}
		}

		$count = 0;
		$data = array();
		if($like)
		{
			$count = Model_Note::query()
				->where('title', 'LIKE', "%$q%")
				->or_where('body', 'LIKE', "%$q%")
				->count();
			$data = Model_Note::query()
				->where('title', 'LIKE', "%$q%")
				->or_where('body', 'LIKE', "%$q%")
				->order_by($order_col, ($da) ? 'ASC' : 'DESC')
				->offset($offset)->limit($limit)
				->get();
		}
		else
		{
			$query = "SELECT *, MATCH (title,body) AGAINST ($q IN BOOLEAN MODE)";
			$query .= " as relevance FROM notes";
			$query .= " WHERE deleted_at IS NULL";
			$query .= " HAVING relevance > 0";
			$query .= " ORDER BY $order_col " . (($da) ? 'ASC' : 'DESC');
			$query .= " LIMIT $offset, $limit";

			$result = DB::query(DB::expr($query), \DB::SELECT)->execute();
			foreach ($result as $d)
			{
				$data[] = Model_Note::forge($d);
			}

			$count = DB::count_last_query();
		}

		return array('count' => $count, 'notes' => $data);
	}


	public static function escape_like($s, $e)
	{
		return str_replace(array($e, '_', '%'), array($e . $e, $e . '_', $e . '%'), $s);
	}


	public static function escape_match($s, $e)
	{
		// TODO: find out if we need to escape characters when using 'MATCH AGAINST'
		return DB::escape($s); //str_replace(array($e, '_', '%'), array($e . $e, $e . '_', $e . '%'), $s);
	}
}
