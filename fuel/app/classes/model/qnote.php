<?php
use Orm\Model_Soft;

class Model_Qnote extends Model_Soft
{
	const LANGS = array(
		'bash'       => 'bash',
		'csharp'     => 'c#',
		'css'        => 'css',
		'cpp'        => 'c++',
		'git'        => 'git',
		'html'       => 'html',
		'java'       => 'java',
		'javascript' => 'javascript',
		'perl'       => 'perl',
		'php'        => 'php',
		'plain'      => 'plain',
		'python'     => 'python',
		'ruby'       => 'ruby',
		'sql'        => 'sql',
	);

	const BRUSHES = array(
		'bash'       => 'bash',
		'c#'         => 'csharp',
		'css'        => 'css',
		'cpp'        => 'cpp',
		'git'        => 'bash',
		'html'       => 'plain',
		'java'       => 'java',
		'javascript' => 'javascript',
		'perl'       => 'perl',
		'php'        => 'php',
		'plain'      => 'plain',
		'python'     => 'python',
		'ruby'       => 'ruby',
		'sql'        => 'sql',
	);

	protected static $_properties = array(
		'id',
		'body',
		'lang',
		'created_at',
		'updated_at',
		'deleted_at',
	);

	protected static $_observers = array(
		'Orm\Observer_CreatedAt' => array(
			'events' => array('before_insert'),
			'mysql_timestamp' => false,
		),
		'Orm\Observer_UpdatedAt' => array(
			'events' => array('before_save'),
			'mysql_timestamp' => false,
		),
	);

	protected static $_soft_delete = array(
			'mysql_timestamp' => false,
	);

	public static function validate($factory)
	{
		$val = Validation::forge($factory);
		$val->add_field('body', 'Body', 'required|min_length[10]');
		$val->add_field('lang', 'Lang', 'max_length[20]');

		return $val;
	}

	/**
	 * @param string $q Query to search for
	 * @param int    $offset Offset (for pagination)
	 * @param int    $limit  Limit
	 * @param string $order_col column to order by
	 * @param bool   $da   DESC or ASC
	 * @param bool   $like Whether or not to use LIKE of mysql (otherwise MATCH AGAINST will be used)
	 * @return array Array in format {'count'=>int, 'notes'=>array}
	 */
	public static function search($q = '', $offset = 0, $limit = 0, $like = false)
	{
		if(is_null($q) || $q === '')
		{
			return array(); 
		} else {
			$q = ($like) ? Model_Note::escape_like($q, '\\') : DB::escape($q);
			if(!$q || $q === '')
			{
				return array();
			}
		}

		$data = array();
		if($like)
		{
			$data = Model_Qnote::query()
				->where('lang', 'LIKE', "%$q%")
				->or_where('body', 'LIKE', "%$q%")
				->order_by('updated_at', 'DESC')
				->offset($offset)->limit($limit)
				->get();
		}
		else
		{
			$query = "SELECT *, MATCH (lang,body) AGAINST ($q IN BOOLEAN MODE)";
			$query .= " as relevance FROM qnotes";
			$query .= " WHERE deleted_at IS NULL";
			$query .= " HAVING relevance > 0";
			$query .= " ORDER BY relevance DESC";
			$query .= " LIMIT $offset, $limit";
	
			$result = DB::query(DB::expr($query), \DB::SELECT)->execute();
			foreach ($result as $d)
			{
				$data[] = Model_Qnote::forge($d);
			}
		}
		return $data;
	}

}
