<?php

class AppSecurity 
{
	public static function textsanitize($html)
	{
		return preg_replace('#<script(.*?)>(.*?)</script>#is', '', $html);
	}

	public static function shorten_html($html, $len = 200)
	{
		$res = $html;
		$res = mb_strimwidth($res, 0, $len, "...");
		$res = strip_tags($res, "<h1><h2><h3><h4><h5><h6><p><br><a><img>");
		return $res;
	}
}


?>