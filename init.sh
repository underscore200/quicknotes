# If you want to use IDE
# download https://gist.github.com/5000164/a7731d2e151c664bef13#file-autocomplete-php
# and add it to 'fuel/app/tasks/' folder
# then run
# > php oil refine autocomplete

# Create ORM scaffolds
echo "Scaffolding  'note'"
php oil g scaffold note title:string body:text category_id:integer is_public:enum[0,1] author_id:integer
echo "Edit contents of 'fuel/app/migrations/001_create_notes.php' for migration to database (default values, NULL or NOT NULL, etc)"
echo "Extend 'Orm\Model_soft' and add 'deleted_at' to model and migration in order to implement soft_delete"

echo "Scaffolding 'category'"
php oil g scaffold category name:string description:string

echo "Scaffolding 'tags'"
php oil g scaffold tags count:int name:string

echo "Generating note-tag relation models"
php oil g model note_tag note_id:int tag_id:int

echo "Please apply migration manually by 'php oil refine migrate' after changing migration files"
echo "Also seed by using 'php oil refine seedTags' and 'php oil refine seedCategories'"
